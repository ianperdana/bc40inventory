﻿using Inventory.Business.Interfaces.CashBanks;
using Inventory.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Business.Repositories.CashBanks
{
    public class CashBankRepository : ICashBankRepository
    {
        private readonly Bc40inventoryContext _dbContext;
        public List<CashBank> getAllCashBanks()
        {
            return _dbContext.CashBanks.ToList();
        }
    }
}
