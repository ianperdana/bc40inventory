﻿using Inventory.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Business.Interfaces.CashBanks
{
    public interface ICashBankRepository
    {
        public List<CashBank> getAllCashBanks();
    }
}
