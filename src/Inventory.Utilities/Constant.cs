﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Utilities
{
    public static class Constant
    {
        public static int ITEMS_PER_PAGE { get; } = 10;
    }
}
