﻿using Inventory.Business.Interfaces.CashBanks;
using Inventory.Business.Repositories.CashBanks;
using Inventory.Presentation.Api.CashBanks.Services;

namespace Inventory.Presentation.Api.Configurations
{
    public static class ConfigurationBusiness
    {
        public static IServiceCollection AddConfigurationBusiness(this IServiceCollection service)
        {
            //Repository
            service.AddScoped<ICashBankRepository, CashBankRepository>();

            //Service
            service.AddScoped<CashBankService>();

            return service;
        }
    }
}
