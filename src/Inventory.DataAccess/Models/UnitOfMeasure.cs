﻿using System;
using System.Collections.Generic;

namespace Inventory.DataAccess.Models;

public partial class UnitOfMeasure
{
    public int Id { get; set; }

    public string? UnitOfMeasureName { get; set; }

    public string? Description { get; set; }

    public DateTime CreateDate { get; set; }

    public string CreateBy { get; set; } = null!;

    public DateTime? UpdateDate { get; set; }

    public string UpdateBy { get; set; } = null!;

    public bool? IsDelete { get; set; }

    public virtual ICollection<Product> Products { get; set; } = new List<Product>();
}
