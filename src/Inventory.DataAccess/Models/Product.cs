﻿using System;
using System.Collections.Generic;

namespace Inventory.DataAccess.Models;

public partial class Product
{
    public int Id { get; set; }

    public string ProductName { get; set; } = null!;

    public int? UomId { get; set; }

    public string? Barcode { get; set; }

    public string? Description { get; set; }

    public decimal? BuyingPrice { get; set; }

    public decimal? SellingPrice { get; set; }

    public int? BranchId { get; set; }

    public string? CurrencyCode { get; set; }

    public string CreatedBy { get; set; } = null!;

    public DateTime CreatedDate { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public bool IsDeleted { get; set; }

    public virtual Branch? Branch { get; set; }

    public virtual Currency? CurrencyCodeNavigation { get; set; }

    public virtual UnitOfMeasure? Uom { get; set; }
}
