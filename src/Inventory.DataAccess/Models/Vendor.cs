﻿using System;
using System.Collections.Generic;

namespace Inventory.DataAccess.Models;

public partial class Vendor
{
    public long VendorId { get; set; }

    public string VendorName { get; set; } = null!;

    public long VendorType { get; set; }

    public string Address { get; set; } = null!;

    public string? City { get; set; }

    public string? State { get; set; }

    public int? ZipCode { get; set; }

    public string? Phone { get; set; }

    public string Email { get; set; } = null!;

    public string ContactPerson { get; set; } = null!;

    public string CreatedBy { get; set; } = null!;

    public DateTime? CreatedDate { get; set; }

    public string ModifiedBy { get; set; } = null!;

    public DateTime? ModifiedDate { get; set; }

    public bool IsDeleted { get; set; }

    public virtual VendorType VendorTypeNavigation { get; set; } = null!;
}
