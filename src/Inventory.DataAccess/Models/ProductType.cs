﻿using System;
using System.Collections.Generic;

namespace Inventory.DataAccess.Models;

public partial class ProductType
{
    public int Id { get; set; }

    public string ProductTypeName { get; set; } = null!;

    public string? Description { get; set; }

    public string? CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public bool IsDeleted { get; set; }
}
