﻿using System;
using System.Collections.Generic;

namespace Inventory.DataAccess.Models;

public partial class CustomerType
{
    public int Id { get; set; }

    public string CustomerType1 { get; set; } = null!;

    public string? Description { get; set; }

    public string? CreatedBy { get; set; }

    public DateTime CreatedDate { get; set; }

    public string? ModifiedBy { get; set; }

    public DateTime? ModifiedDate { get; set; }

    public bool IsDeleted { get; set; }

    public virtual ICollection<Customer> Customers { get; set; } = new List<Customer>();
}
