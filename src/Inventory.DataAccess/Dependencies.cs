﻿using Inventory.DataAccess.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace Inventory.DataAccess
{
    public class Dependencies
    {
        public static void AddDataAccessServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<Bc40inventoryContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("Bc40InventoryConnection"))
            );
        }
    }
}
